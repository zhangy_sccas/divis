#!/usr/bin/env python3
# coding:utf-8

import time
import shlex

import click

from tools.filecheck import check_md5values
from tools.command import is_in_commands, run_command
from tools.caltime import show_runtime
from tools.qc import qc_step
from tools.align import align_step
from tools.pipeline import do_pipeline


@click.group()
def cli():
    pass


@click.command()
@click.argument('command', nargs=1, type=str)
def run(command):
    """run other third commands."""
    command_splits = shlex.split(command)
    if is_in_commands(command_splits[0]):
        start_time = time.time()
        result = run_command(command_splits)
        if result is False:
            exit(-1)
        end_time = time.time()
        show_runtime(int(end_time - start_time))
    else:
        click.echo("{} is not allowed".format(command_splits[0]))


@click.command()
@click.argument("dir")
def check(dir):
    """check md5 values of sequencing data."""
    check_md5values(dir)


@click.command()
@click.option('--forward', '-f', required=True, help="Forward reads file")
@click.option('--reverse', '-r', required=True, help="Reverse reads file")
@click.option('--out_dir', '-o', required=True, help="Output directory")
def qc(forward, reverse, out_dir):
    """do quality control workflow."""
    qc_step(forward, reverse, out_dir)


@click.command()
@click.option('--name', '-n', required=True, help="ouput name")
@click.option('--forward', '-r', required=True, help="Forward reads file")
@click.option('--reverse', '-r', required=True, help="Reverse reads file")
@click.option('--id', required=True, help="id")
@click.option('--sm', required=True, help="sample name")
@click.option('--lb', required=True, help="library name")
@click.option('--pl', required=True, help="platform,eg:Illumina")
@click.option('--out_dir', '-o', required=True, help="Output directory")
def align(name, forward, reverse, id, sm, lb, pl, out_dir):
    """do alignment workflow."""
    align_step(name, forward, reverse, id, sm, lb, pl, out_dir)


@click.command()
@click.option('--config', required=True, help="Job config")
def do(config):
    """do full workflow."""
    do_pipeline(config)


cli.add_command(run)
cli.add_command(check)
cli.add_command(qc)
cli.add_command(align)
cli.add_command(do)

if __name__ == '__main__':
    cli()
