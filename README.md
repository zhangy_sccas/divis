# What's DIVIS
DIVIS is a workflow that includes a a series of bioinformatic tools and can help you to analyse human genome and
find variance more convenient.

# Installation
As a complex pipline, we recommend you to run it on docker.

## Local Installation
### Requirements
- Ubuntu 16.04
- Python 3.3+
### Installation Shellscript
Please Wait...


We will release the installation script later.


## Docker Installation
```
docker pull lovestarzy/divis
```

# Run
```
docker run -v [Your Work Data Directory]:/data -it lovelyzhang/divis
```

# How to Use DIVIS
Currently, DIVIS includes the pipeline from QC step to Alignment Step.
We will develop the other steps later. Please follow us. Thanks.

## Config DIVIS
Please config DIVIS before run it. You can find the config template in `templates/` which is in json format.
Copy the config to `config/divis.json`.
In `divis.json` you should tell divis the path of essential tools, reference genome and so on.

## Run full pipeline
```
./divis.py do --config [job-config]
```
job-config: the arguments you will submit to DIVIS. You can write your own job config from `templates/job.tpl.json`.

## Run each step by yourself
1. Use `run` sub-command.


By this means, You can run any tool you want, and give arguments to the tools you like.
```
./divis.py run [tool command]
```
2. use step sub-command


By this means, You can run the step by our default arguments.
- QC step


For more information, please see help.
```
./divis.py qc --help
```

- Alignment step


For more information, please see help.
```
./divis.py align --help
```