import json
import os
from tools.qc import qc_step
from tools.align import align_step
from tools.proc import worker_showtime


def full_pipeline(job_config):
    if not os.path.exists(job_config):
        err = "{job} not exist.".format(job=job_config)
        raise Exception(err)

    with open(job_config, 'r') as cfg:
        job = json.load(cfg)

    clean_r1, clean_r2 = qc_step(r1=job["R1Filepath"], r2=job["R2Filepath"], output_dir=job["QCReportDir"])

    align_step(job["Name"], f1=clean_r1, f2=clean_r2,
               id=job["ID"],
               sm=job["SampleName"],
               lb=job["LibraryName"],
               pl=job["PlatformName"],
               output_dir=job["AlignmentOutputDir"])


def do_pipeline(job_config):
    # worker_showtime(full_pipeline, job_config)
    full_pipeline(job_config)
