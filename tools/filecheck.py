# coding:utf-8

import os
import subprocess
import shlex


def read_md5values(filepath):
    md5_dict = dict()
    with open(filepath, 'r') as md5_values_file:
        for line in md5_values_file:
            splits = line.split()
            md5_dict[splits[1]] = splits[0]
    return md5_dict


def check_md5values(data_dir):
    if os.path.exists(data_dir):
        md5values_path = os.path.join(data_dir, "md5.values")
        if os.path.exists(md5values_path):
            md5_dict = read_md5values(md5values_path)
        filenames = os.listdir(data_dir)
        filenames.remove("md5.values")

        for filename in filenames:
            fullpath = os.path.join(data_dir, filename)
            if os.path.isfile(fullpath):
                exec_str = "md5sum {filename}".format(filename=fullpath)
                rt_str = subprocess.check_output(shlex.split(exec_str)).decode('utf-8').replace('\n', "")
                splits = rt_str.split()
                _, splits[1] = os.path.split(splits[1])
                if md5_dict[splits[1]] != splits[0]:
                    print("{filename} check failed!".format(filename=filename))
                else:
                    print("{filename} check through!".format(filename=filename))
    else:
        print("{dir} not found.".format(dir=data_dir))
