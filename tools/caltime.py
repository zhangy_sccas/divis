# coding:utf-8


def show_runtime(runtime, show_type):
    hours = runtime // 3600
    minutes = (runtime - hours * 3600) // 60
    seconds = (runtime - hours * 3600 - minutes * 60)
    print("{show_type} {hours} hours {minutes} minutes {seconds} seconds".format(show_type=show_type,
                                                                                 hours=hours,
                                                                                 minutes=minutes,
                                                                                 seconds=seconds))


def total_runtime(runtime):
    show_runtime(runtime, show_type="Total run:")


def current_runtime(runtime):
    show_runtime(runtime, show_type="Already run:")
