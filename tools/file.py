import os


def dir_check(dir_path):
    return os.path.exists(os.path.abspath(dir_path))


def dir_create(dir_path):
    if not os.path.exists(os.path.abspath(dir_path)):
        os.mkdir(os.path.abspath(dir_path))


def filepath_dir_create(filepath):
    dir_path = os.path.dirname(filepath)
    dir_create(dir_path)


def get_file_basename(filepath):
    abs_path = os.path.abspath(filepath)
    basename = os.path.basename(abs_path)
    return basename


def get_filename_with_no_extension(filepath):
    return get_file_basename(filepath).split('.')[0]


if __name__ == '__main__':
    print(get_file_basename("/data/pair1.fastq"))