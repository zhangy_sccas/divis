from multiprocessing import Process
import time
import logging

from tools.caltime import current_runtime, total_runtime

max_run_time = 24 * 60 * 60 * 3
show_interval = 10


def worker_with_limit_time(func, *args, **kwargs):
    proc = Process(target=func, args=args, kwargs=kwargs)
    proc.start()
    start_time = time.time()
    while proc.is_alive():
        time.sleep(1)
        run_time = int(time.time() - start_time)
        if run_time > max_run_time:
            proc.terminate()
            logging.fatal("Error:run pipeline exceed max limit time.")


def worker(func, *args, **kwargs):
    proc = Process(target=func, args=args, kwargs=kwargs)
    proc.start()
    proc.join()


def worker_showtime(func, *args, **kwargs):
    proc = Process(target=func, args=args, kwargs=kwargs)
    proc.start()
    start_time = time.time()
    while proc.is_alive():
        time.sleep(1)
        run_time = int(time.time() - start_time)
        if run_time >= 60 and run_time % 60 == 0:
            current_runtime(run_time)
        if run_time > max_run_time:
            proc.terminate()
    total_runtime(run_time)
