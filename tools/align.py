import os
from tools.config import DIVIS_CONFIG, DEFAULT_THREADS, REF_GENOME
from tools.file import dir_create
from tools.command import run_command
from tools.proc import worker

samtools_config = DIVIS_CONFIG["samtools"]
bwa_config = DIVIS_CONFIG["bwa"]
picard_config = DIVIS_CONFIG["picard"]

samtools_bin = os.path.join(samtools_config["path"], "samtools")
bwa_bin = os.path.join(bwa_config["path"], "bwa")
picard_bin = os.path.join(picard_config["path"], "picard.jar")


def bwa_index_create():
    if os.path.exists("{}.amb".format(REF_GENOME)) \
            and os.path.exists("{}.ann".format(REF_GENOME)) \
            and os.path.exists("{}.bwt".format(REF_GENOME)) \
            and os.path.exists("{}.pac".format(REF_GENOME)) \
            and os.path.exists("{}.sa".format(REF_GENOME)):
        return
    cmd = "{bwa} index {ref}".format(bwa=bwa_bin,
                                     ref=REF_GENOME)
    worker(run_command, cmd)


def sam_to_bam(samfile, bamfile):
    cmd = "{samtools} view -bS {samfile} -o {bamfile}".format(samtools=samtools_bin,
                                                              samfile=samfile,
                                                              bamfile=bamfile)
    worker(run_command, cmd)


def bwa_alignment(r, f1, f2, output):
    """bwa mem"""

    cmd = "{bwa} mem -t {threads} -M -R '{r}' {ref} {f1} {f2} -o {output}".format(bwa=bwa_bin,
                                                                                  threads=DEFAULT_THREADS,
                                                                                  r=r,
                                                                                  ref=REF_GENOME,
                                                                                  f1=f1,
                                                                                  f2=f2,
                                                                                  output=output)
    worker(run_command, cmd)


def bam_sort(input, output):
    """picard bam sort"""

    cmd = "java -jar {picard} SortSam \
    INPUT={input} OUTPUT={output} \
    SORT_ORDER=queryname".format(picard=picard_bin, input=input, output=output)
    worker(run_command, cmd)


def get_raw_bam(f1, f2, output, rg, sm, lb, pl):
    """picard get raw bam"""

    cmd = "java -jar {picard} FastqToSam F1={f1} F2={f2} \
    OUTPUT={output} RG={rg} SM={sm} LB={lb} PL={pl} \
    SORT_ORDER=queryname".format(picard=picard_bin,
                                 f1=f1,
                                 f2=f2,
                                 output=output,
                                 rg=rg,
                                 sm=sm,
                                 lb=lb,
                                 pl=pl)
    worker(run_command, cmd)


def create_ref_dict():
    if os.path.exists("{}.dict".format(REF_GENOME)):
        return
    cmd = "java -jar {picard} CreateSequenceDictionary R={ref} O={ref}.dict".format(picard=picard_bin,
                                                                                    ref=REF_GENOME)
    worker(run_command, cmd)


def bam_merge(alingend_bam, unmapped_bam, output):
    """picard bam merge"""

    cmd = '''java -jar {picard} MergeBamAlignment ALIGNED_BAM={alingend_bam} \
    UNMAPPED_BAM={unmapped_bam} \
    OUTPUT={output} \
    REFERENCE_SEQUENCE={ref} \
    SORT_ORDER=coordinate \
    ADD_MATE_CIGAR=true \
    CLIP_ADAPTERS=false \
    CLIP_OVERLAPPING_READS=true \
    INCLUDE_SECONDARY_ALIGNMENTS=true \
    MAX_INSERTIONS_OR_DELETIONS=-1 \
    PRIMARY_ALIGNMENT_STRATEGY=MostDistant \
    ATTRIBUTES_TO_RETAIN=XS PAIRED_RUN=true \
'''.format(picard=picard_bin,
           alingend_bam=alingend_bam,
           unmapped_bam=unmapped_bam,
           ref=REF_GENOME,
           output=output)
    worker(run_command, cmd)


def mark_duplicats(input, output, metrix_output):
    """picard mark duplicates"""

    cmd = '''java -jar {picard} MarkDuplicates INPUT={input} OUTPUT={output} METRICS_FILE={metrix_output}\
    MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=8000 \
    REMOVE_DUPLICATES=false
    '''.format(picard=picard_bin,
               input=input,
               output=output,
               metrix_output=metrix_output)
    worker(run_command, cmd)


def align_step(name, f1, f2, id, sm, lb, pl, output_dir):
    bwa_R = "@RG\\tID:{id}\\tSM:{sm}\\tLB:{lb}\\tPL:{pl}".format(id=id, lb=lb, sm=sm, pl=pl)

    output_dir = os.path.abspath(output_dir)
    dir_create(os.path.abspath(output_dir))
    sam_dir = os.path.join(output_dir, "sam")
    bam_dir = os.path.join(output_dir, "bam")
    dir_create(sam_dir)
    dir_create(bam_dir)

    alinged_sam = os.path.join(sam_dir, "{filename}.aligned.sam".format(filename=name))
    alinged_bam = os.path.join(bam_dir, "{filename}.aligned.bam".format(filename=name))
    sorted_bam = os.path.join(bam_dir, "{filename}.aligned.sorted.bam".format(filename=name))
    raw_bam = os.path.join(bam_dir, "{filename}.raw.bam".format(filename=name))
    merged_bam = os.path.join(bam_dir, "{filename}.bam".format(filename=name))
    dedupped_bam = os.path.join(bam_dir, "{filename}.dedupped.bam".format(filename=name))
    metrics_file = os.path.join(bam_dir, "{filename}.dedupped.metrics".format(filename=name))

    bwa_index_create()
    bwa_alignment(r=bwa_R, f1=f1, f2=f2, output=alinged_sam)
    sam_to_bam(alinged_sam, alinged_bam)
    bam_sort(alinged_bam, sorted_bam)
    get_raw_bam(f1=f1, f2=f2, output=raw_bam, rg=id, sm=sm, lb=lb, pl=pl.upper())
    create_ref_dict()
    bam_merge(alinged_bam, raw_bam, merged_bam)
    mark_duplicats(merged_bam, dedupped_bam, metrics_file)
    return os.path.abspath(dedupped_bam)
