# coding:utf-8
import shlex
import sys
import logging
from subprocess import Popen, PIPE, STDOUT

third_commands = ["fastqc", "bwa"]


def is_in_commands(command):
    return command in third_commands


def run_command(cmdline):
    logging.info("Run:{}".format(cmdline))
    popen = Popen(shlex.split(cmdline), stdin=PIPE, stdout=PIPE, stderr=STDOUT)
    while popen.poll() is None:
        while True:
            outs = popen.stdout.read(1).decode('utf-8')
            if outs is '':
                break
            sys.stdout.write(outs)
    if popen.returncode != 0:
        return False
    return True
