# coding:utf-8
import json
import multiprocessing

CONFIG_PATH = "config/divis.json"
with open(CONFIG_PATH, 'r') as cfg:
    DIVIS_CONFIG = json.load(cfg)

REF_GENOME = DIVIS_CONFIG["RefGenome"]

SYSTEM_HALF_THREADS = multiprocessing.cpu_count() / 2
# DEFAULT_THREADS = DIVIS_CONFIG["WorkThreads"] \
#     if DIVIS_CONFIG["WorkThreads"] > SYSTEM_HALF_THREADS else SYSTEM_HALF_THREADS

DEFAULT_THREADS = DIVIS_CONFIG["WorkThreads"]
