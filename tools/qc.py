# coding:utf-8

import os

from tools.command import run_command
from tools.config import DIVIS_CONFIG, DEFAULT_THREADS
from tools.proc import worker

fastqc_config = DIVIS_CONFIG["fastqc"]
fastqc_bin = os.path.join(fastqc_config["path"], "fastqc")
nqsqc_config = DIVIS_CONFIG["nqsqc"]
nqsqc_bin = os.path.join(nqsqc_config["path"], "IlluQC_PRLL.pl")


def fastqc(r1, r2, output):
    cmd = "{fastqc} -t {thread} -o {output} {r1} {r2}".format(fastqc=fastqc_bin,
                                                              thread=DEFAULT_THREADS,
                                                              r1=r1,
                                                              r2=r2,
                                                              output=output
                                                              )

    worker(run_command, cmd)


def nqsqc(r1, r2, output):
    cmd = "perl {nqsqc} -pe {r1} {r2} {library} {variant} -c {cpus} -o {output}".format(nqsqc=nqsqc_bin,
                                                                                        r1=r1,
                                                                                        r2=r2,
                                                                                        library=nqsqc_config[
                                                                                            "library"],
                                                                                        variant=nqsqc_config[
                                                                                            "variant"],
                                                                                        cpus=DEFAULT_THREADS,
                                                                                        output=output
                                                                                        )
    worker(run_command, cmd)


def qc_step(r1, r2, output_dir):
    # if output_dir no exist creat directories.
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    before_r_d = os.path.join(output_dir, "before")
    after_r_d = os.path.join(output_dir, "after")
    clean_dir = os.path.join(output_dir, "clean")

    if not os.path.exists(before_r_d):
        os.mkdir(before_r_d)
    if not os.path.exists(after_r_d):
        os.mkdir(after_r_d)
    if not os.path.exists(clean_dir):
        os.mkdir(clean_dir)

    # do fastqc
    fastqc(r1, r2, before_r_d)
    # do nqsqc
    nqsqc(r1, r2, output_dir)

    r1_name = os.path.basename(r1)
    r2_name = os.path.basename(r2)
    out_r1 = os.path.join(output_dir, "{}_filtered".format(r1_name))
    out_r2 = os.path.join(output_dir, "{}_filtered".format(r2_name))

    # do clean data fastqc
    fastqc(out_r1, out_r2, after_r_d)

    # mv clean data to clean directory
    cmd = "mv -t {dir} {source}".format(dir=os.path.abspath(clean_dir),
                                        source=" ".join([os.path.abspath(out_r1), os.path.abspath(out_r2)]))
    worker(run_command, cmd)

    # rename _filtered file to .fq file
    out_r1_clean_path = os.path.join(clean_dir, "{}_filtered".format(r1_name))
    out_r2_clean_path = os.path.join(clean_dir, "{}_filtered".format(r2_name))
    out_r1_clean_fastq_path = os.path.join(clean_dir, "{}.fq".format(r1_name))
    out_r2_clean_fastq_path = os.path.join(clean_dir, "{}.fq".format(r2_name))
    cmd = "mv {src} {des}".format(src=out_r1_clean_path,
                                  des=out_r1_clean_fastq_path)
    worker(run_command, cmd)
    cmd = "mv {src} {des}".format(src=out_r2_clean_path,
                                  des=out_r2_clean_fastq_path)
    worker(run_command, cmd)
    return out_r1_clean_fastq_path, out_r2_clean_fastq_path
